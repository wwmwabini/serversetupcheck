#!/bin/bash
#Script to test various aspects of a production server
#Version 1.0.0 Dec 2023
#By Wallace Mwabini, wallace@ryanada.com



EXISTS_STATUS="EXISTS"
MISSING_STATUS="MISSING"

COUNTER=0
TOTAL_TESTS=54

DEFAULT_COLOR="\e[0m"
GREEN_COLOR="\e[32m"
RED_COLOR="\e[31m"
YELLOW_COLOR="\e[33m"

echo -e "$DEFAULT_COLOR"


# 0. BASIC SERVER DETAILS
echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR**************Basic Server Details**************$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"

echo -e "Hostname: `hostname`" 
echo -e "IPv4 Addresses: `hostname -I`" 
echo -e "Uptime: `uptime`"

# 1. CHECK MySQL SETTINGS

echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR**************Checking MySQL Configuration Settings**************$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"

MYSQL_CONFIG_FILE="/etc/my.cnf"


performance_schema="performance-schema=0"
sql_mode="sql_mode=NO_ENGINE_SUBSTITUTION"
event_scheduler="event_scheduler=off"
max_connections="max_connections=300"
max_user_connections="max_user_connections=50"
wait_timeout="wait_timeout=100"
interactive_timeout="interactive_timeout=150"
sort_buffer_size="sort_buffer_size=1M"
query_cache_type="query_cache_type=1"
query_cache_size="query_cache_size=100M"
innodb_buffer_pool_size="innodb_buffer_pool_size=8192M"


grep -q "$performance_schema" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $performance_schema \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $performance_schema \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$sql_mode" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $sql_mode \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $sql_mode \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$event_scheduler" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $event_scheduler \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $event_scheduler \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$max_connections" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $max_connections \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $max_connections \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$max_user_connections" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $max_user_connections \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $max_user_connections \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$wait_timeout" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $wait_timeout \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $wait_timeout \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$interactive_timeout" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $interactive_timeout \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $interactive_timeout \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$sort_buffer_size" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $sort_buffer_size \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $sort_buffer_size \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$query_cache_type" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $query_cache_type \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $query_cache_type \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$query_cache_size" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $query_cache_size \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $query_cache_size \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi


grep -q "$innodb_buffer_pool_size" "$MYSQL_CONFIG_FILE"
if [ $? -eq 0 ];then
	echo -e "- Setting $innodb_buffer_pool_size \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	((COUNTER++))
else
	echo -e "- Setting $innodb_buffer_pool_size \n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi





# 2. CHECK PHP.ini SETTINGS


echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR**************Checking PHP.ini Configuration Settings**************$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"

PHP_VERSIONS=(56 70 71 72 73 74 80 81 82)

memory_limit="memory_limit = 256M"
post_max_size="post_max_size = 32M"
upload_max_filesize="upload_max_filesize = 32M"
disable_functions="disable_functions = 'allow_url_fopen, show_source, system, shell_exec, passthru, exec, phpinfo,  mail, mysql_list_dbs, ini_alter, dl, symlink, link, chgrp, leak, popen, ap'"

for current_php_version in "${PHP_VERSIONS[@]}"
do
	echo -e "$YELLOW_COLOR== Checking eaphp-$current_php_version ==$DEFAULT_COLOR"
	path=/opt/cpanel/ea-php$current_php_version/root/etc/php.ini

	grep -q "$memory_limit" "$path"
	if [ $? -eq 0 ]; then
		echo -e "- Setting $memory_limit \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	    ((COUNTER++))
	else
		echo -e "- Setting $memory_limit :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
	fi

	grep -q "$post_max_size" "$path"
	if [ $? -eq 0 ]; then
		echo -e "- Setting $post_max_size \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	    ((COUNTER++))
	else
		echo -e "- Setting $post_max_size :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
	fi


	grep -q "$upload_max_filesize" "$path"
	if [ $? -eq 0 ]; then
		echo -e "- Setting $upload_max_filesize \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	    ((COUNTER++))
	else
		echo -e "- Setting $upload_max_filesize :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
	fi


	grep -q "$disable_functions" "$path"
	if [ $? -eq 0 ]; then
		echo -e "- Setting $disable_functions \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	    ((COUNTER++))
	else
		echo -e "- Setting $disable_functions :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
	fi

done



#3. DNS CONFIGURATION TEST

domain='example.com'
username="pqead7ivb"
password=`head -c 10 /dev/urandom | sha1sum | cut -d' ' -f1`

# create account for test

echo -e "Creating test account..."
whmapi1 --output=jsonpretty createacct username=$username domain=$domain password=$password > /tmp/server_setup_check.log
echo -e "Done..."

if [ $? -eq 0 ]; then
	echo -e "$GREEN_COLOR Account creation OK.$DEFAULT_COLOR"
	((COUNTER++))
else 
	echo -e "$RED_COLOR Account creation FAILED.$DEFAULT_COLOR"
fi


/usr/local/cpanel/scripts/dnscluster synczone $domain


if [ -f /var/cpanel/useclusteringdns ]; then
	echo -e "- $GREEN_COLOR Good. DNS Cluster is configured.$DEFAULT_COLOR"
	((COUNTER++))

	NAMESERVERS=(ns1.cloudoon.com ns2.cloudoon.net ns3.cloudoon.org)

	for ns in "${NAMESERVERS[@]}"
	do
		query=`dig +tries=2 +time=5 @$ns $domain +short`

		if [[ $query =~ ^(([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))\.){3}([1-9]?[0-9]|1[0-9][0-9]|2([0-4][0-9]|5[0-5]))$ ]]; then
			echo -e "- $GREEN_COLOR Great! $domain is synced into $ns ... $DEFAULT_COLOR"
			((COUNTER++))
		else
			echo -e "- $RED_COLOR Oops! $domain is NOT synced into $ns ... $DEFAULT_COLOR"
		fi

	done
else 
	echo -e "- $RED_COLOR Error. DNS Cluster is NOT configured.$DEFAULT_COLOR"
fi

# remove account after test
echo -e "Deleting test account..."
/usr/local/cpanel/scripts/removeacct --force $username > /tmp/server_setup_check.log
echo -e "Done..."
echo -e "Deleting $domain DNS records if any..."
/usr/local/cpanel/scripts/killdns --force $domain > /tmp/server_setup_check.log
echo -e "Done..."




#4. Jetbackup Configuration

#check if jetbackup is installed

jetbackup5api -h > /dev/null 2>&1

if [ $? -eq 0 ]; then
	echo -e "- Jetbackup 5 software \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
    ((COUNTER++))

    jetbackup5api -F listDestinations | grep _id > /tmp/jetbackupdestinations.txt

	mapfile -t destinations < /tmp/jetbackupdestinations.txt

	if [ -s /tmp/jetbackupdestinations.txt ]; then
	    echo -e "- Jetbackup 5 Destinations \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
    	((COUNTER++))

    	for destination in "${destinations[@]}"; do

			FINAL_DESTINATION=${destination#*_id: }

			RESULT=`jetbackup5api -F validateDestination -D "_id=${FINAL_DESTINATION}"`

			if [[ $RESULT == *"Destination Validated Successfully"* ]]; then
			    echo -e "- Jetbackup 5 Destination \n $GREEN_COLOR[$EXISTS_STATUS]$DEFAULT_COLOR"
	    		((COUNTER++))
			else
			    echo -e "- Jetbackup 5 Destination :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
			fi
		done

	else
	    echo -e "- Jetbackup 5 Destinations :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
	fi


else
	echo -e "- Jetbackup 5 software :\n $RED_COLOR[$MISSING_STATUS]$DEFAULT_COLOR"
fi




echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR**************Summary**************$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"

PERCENTAGE_PASS=$(echo "scale=2; $COUNTER / $TOTAL_TESTS * 100" | bc)


echo -e "Total tests passed: $COUNTER"
echo -e "Pass Percentage: $PERCENTAGE_PASS%"



echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR**************The End**************$DEFAULT_COLOR"
echo -e "$YELLOW_COLOR=============================================================================================$DEFAULT_COLOR"

